package jTests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import operations.Addition;
import operations.Monom;
import operations.Polinom;
import operations.Subtraction;

class DifferenceTest {
	
	public Polinom newPol() { //method to create new , empty polynomials 
		ArrayList<Monom> newList = new ArrayList<Monom>();
		Polinom p= new Polinom(newList);
		return p;
	}

	@Test
	void test() {
		// should pass!
				// : (14X - 8 ) - (3X - 6) = 11X - 2.
				
				Polinom p1 = newPol();
				Polinom p2 = newPol();
				Polinom p3 = newPol();
				p1.addMonom(new Monom(14,1));
				p1.addMonom(new Monom(-8,0));
				p2.addMonom(new Monom(3,1));
				p2.addMonom(new Monom(-6,0));
				Subtraction test = new Subtraction(p1,p2);
				Polinom pResult = test.subtract();
		
				p3.addMonom(new Monom(11,1));
				p3.addMonom(new Monom(-2,0));
				assertEquals(p3.polToStr(), pResult.polToStr());
				
		       //should fail !
			   //   (14X - 8 ) - (3X - 6) = 11X + 2.
				Polinom p4 = newPol();

				p4.addMonom(new Monom(11,1));
				p4.addMonom(new Monom(2,0));
				assertNotEquals(p4.polToStr(), pResult.polToStr());
	}

}
