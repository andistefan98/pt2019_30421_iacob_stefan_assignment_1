package jTests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import operations.Differentiation;
import operations.Monom;
import operations.Polinom;

class DifferentiateTest {
	
	public Polinom newPol() { //method to create new , empty polynomials 
		ArrayList<Monom> newList = new ArrayList<Monom>();
		Polinom p= new Polinom(newList);
		return p;
	}

	@Test
	void test() {
		//  (3X^5 - 8X^4 + 4)'  =  15X^4 - 32X^3 + 0
		// should pass !

		Polinom p1 = newPol();
		Polinom p2 = newPol();
		Polinom p3 = newPol();		
		
		p1.addMonom(new Monom(3,5));
		p1.addMonom(new Monom(-8,4));
		p1.addMonom(new Monom(4,0));
		
		p2.addMonom(new Monom(15,4));
		p2.addMonom(new Monom(-32,3));
		
		Differentiation test = new Differentiation(p1) ;
		Polinom tResult = test.differentiate();
		assertEquals(p2.polToStr(),tResult.polToStr());
		
		//  (3X^5  -  8X^4  + 4)'  !=  15X^4 - 32X^4
		// should fail !
		p3.addMonom(new Monom(15,4));
		p3.addMonom(new Monom(-32,4));
		
		assertNotEquals(p3.polToStr(),tResult.polToStr());
		
			}

}
