package jTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import operations.Addition;
import operations.Monom;
import operations.Polinom;

class AddTest {
	
	public 

	@Test
	void test() {
		// should pass!
		// : (14X - 8 ) + (3X - 6) = 17X - 14.
		ArrayList<Monom> monoame1 = new ArrayList<Monom>();
		ArrayList<Monom> monoame2 = new ArrayList<Monom>();
		ArrayList<Monom> monoame3 = new ArrayList<Monom>();
		Polinom p1 = new Polinom( monoame1);
		Polinom p2 = new Polinom( monoame2);
		Polinom p3 = new Polinom( monoame3);
		p1.addMonom(new Monom(14,1));
		p1.addMonom(new Monom(-8,0));
		p2.addMonom(new Monom(3,1));
		p2.addMonom(new Monom(-6,0));
		Addition test = new Addition(p1,p2);
		Polinom pResult = test.add();
		p3.addMonom(new Monom(17,1));
		p3.addMonom(new Monom(-14,0));
		assertEquals(p3.polToStr(), pResult.polToStr());
		
		
		//should fail !
		// (14X-8) + (3X-6) = 17X + 14
		ArrayList<Monom> monoame4 = new ArrayList<Monom>();
		Polinom p4 = new Polinom( monoame1);
		p4.addMonom(new Monom(17,1));
		p4.addMonom(new Monom(14,0));
		
		assertNotEquals(p3.polToStr(),p4.polToStr());
		
	}
	
	

}
