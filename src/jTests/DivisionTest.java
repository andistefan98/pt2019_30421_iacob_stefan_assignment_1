package jTests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import operations.Division;
import operations.Monom;
import operations.Polinom;

class DivisionTest {
	
	public Polinom newPol() { //method to create new , empty polynomials 
		ArrayList<Monom> newList = new ArrayList<Monom>();
		Polinom p= new Polinom(newList);
		return p;
	}
	
	public String afisareQR(Polinom p1, Polinom p2) {
		String fin="";
		String st1= p1.polToStr();  
		fin=fin.concat(st1);  // concatenating the resulted quotient 
		fin=fin.concat("  | r: ");
		String st2= p2.polToStr();  //concatenating the resulted remainder 
		fin=fin.concat(st2);
		return fin;      //return a string of the form : (quotient | remainder)
	}

	@Test
	void test() {
		// should pass!
		// : (14X^2 - 8X +3 ) / (3X - 6) = (4.67X + 6.67 ) + 43
		
		Polinom p1 = newPol();
		Polinom p2 = newPol();
		Polinom remainder = newPol();
		Polinom quotient=newPol();
		p1.addMonom(new Monom(14,2));
		p1.addMonom(new Monom(-8,1));
		p1.addMonom(new Monom(3,0));
		p2.addMonom(new Monom(3,1));
		p2.addMonom(new Monom(-6,0));
		
		quotient.addMonom(new Monom(4.67,1));
		quotient.addMonom(new Monom(6.67,0));
		remainder.addMonom(new Monom(43,0));
		String expectedStr = afisareQR(quotient,remainder);
		
		Division test = new Division(p1,p2);
		String pResult = test.divide4q();
       //we compare the strings formed by the concatenation of the quotient and remainder
		assertEquals(expectedStr, pResult);
		
		quotient.monoame.remove(0);
		quotient.addMonom(new Monom(4.66,1)); //we see the difference between 4,66 & 4,67 when it comes
		String unexpectedStr = afisareQR(quotient,remainder); //to 2 decimal places on double format
		assertNotEquals(unexpectedStr,expectedStr);
		
	}

}
