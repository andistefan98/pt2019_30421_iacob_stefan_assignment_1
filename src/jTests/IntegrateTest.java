package jTests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import operations.Differentiation;
import operations.Integration;
import operations.Monom;
import operations.Polinom;

class IntegrateTest {
	
	public Polinom newPol() { //method to create new , empty polynomials 
		ArrayList<Monom> newList = new ArrayList<Monom>();
		Polinom p= new Polinom(newList);
		return p;
	}

	@Test
	void test() {
		//  integrate(3X^5 - 8X^4 + 4)  = 0.5X^6 - (8/5)X^5 + 4X
		// should pass !
		Polinom p1 = newPol();
		Polinom p2 = newPol();
		Polinom p3 = newPol();
		
		p1.addMonom(new Monom(3,5));
		p1.addMonom(new Monom(-8,4));
		p1.addMonom(new Monom(4,0));
		
		
		p2.addMonom(new Monom(0.5,6));
		p2.addMonom(new Monom(-1.6,5));
		p2.addMonom(new Monom(4,1));
		
		Integration test = new Integration(p1) ;
		Polinom tResult = test.integrate();
		assertEquals(p2.polToStr(),tResult.polToStr());
		
		//  integrate(3X^5 - 8X^4 + 4)  = X^6 - 1X^5 + 4X
		// should fail !
		p3.addMonom(new Monom(1,6));
		p3.addMonom(new Monom(-1,5));
		p3.addMonom(new Monom(4,1));
		assertNotEquals(p3.polToStr(),tResult.polToStr());
	}

}
