package jTests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import operations.Monom;
import operations.Polinom;

class PolinomToStrTest {
	
	public Polinom newPol() { //method to create new , empty polynomials 
		ArrayList<Monom> newList = new ArrayList<Monom>();
		Polinom p= new Polinom(newList);
		return p;
	}

	@Test
	void test() {
		//our polynomial will be :  34X^2 + 14X^1
		Polinom p1 =newPol();
		p1.addMonom(new Monom(34,2));
		p1.addMonom(new Monom(14,1));
		
		String st = " + 34.0X^2 + 14.0X^1";  //expected string to be displayed

		assertEquals(st,p1.polToStr());
		
		Polinom pWr= newPol();
		pWr.addMonom(new Monom(34,2));
		pWr.addMonom(new Monom(14,-1));  // -1 instead of +1
		
		assertNotEquals(st,pWr.polToStr());
	}

}
