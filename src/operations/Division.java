package operations;

import java.util.ArrayList;
import javafx.util.*;

public class Division {


	Polinom pol1;
	Polinom pol2;
	
	public Division(Polinom pol1,Polinom pol2) {
		this.pol1 = pol1;
		this.pol2= pol2;
	}
	
	//division of two monoms
	public Monom divide2(Monom m1, Monom m2) {
		 Monom mRes = new Monom(0,0);
		 mRes.setCoef(m1.getCoef()/m2.getCoef());  //the new coefficient is the division of the 2 current coefficients
		 mRes.setExp(m1.getExp() - m2.getExp() );  //the new exp is the difference between the current exponents
		 return mRes;
	  }
	
	//method used to display the output of the operation into the GUI 
	public String afisareQR(Polinom p1, Polinom p2) {
		String fin="";
		String st1= p1.polToStr();  
		fin=fin.concat(st1);  // concatenating the resulted quotient 
		fin=fin.concat("  | r: ");
		String st2= p2.polToStr();  //concatenating the resulted remainder 
		fin=fin.concat(st2);
		return fin;      //return a string of the form : (quotient | remainder)
	}
	
	//method used to check if a given polynomial is null ( either has no elements or has one monomial with coefficient 0
	public boolean checkIfZero(Polinom p1) {
		boolean ok=false;
		if(p1.monoame.size()==0)
			ok=true;
		if(p1.monoame.size()==1) {
			if(p1.monoame.get(0).getCoef()==0) {
				ok=true;
			}
		}
		return ok;
	}

   
    public int getmaxRank(Polinom p1) { //getting the rank of a given polynomial
    	int maxPow =0;
    	for(Monom m1: p1.monoame) { //taking one exponent at a time to see if it is the maximum so far
    		if(m1.getExp()> maxPow)
    			maxPow = m1.getExp(); //store the new maximum
    	}
    	return maxPow;
    }
    
    public Monom getMonomMax(Polinom p1) { //getting the monomial which gives the rank of the polynomial
    	int maxPow =0;
    	Monom m= new Monom(0,0);
    	for(Monom m1: p1.monoame) { 
    		if(m1.getExp()> maxPow && m1.getExp() != 0) {
    			maxPow = m1.getExp(); 
    			m.setCoef(m1.getCoef()); //update the coefficient atached to the maximum exponent
    			m.setExp(m1.getExp());   //update the max exponent
    	}
    	   }
    	return m;
    }
    	
  //method that actually returns a string 
    public String divide4q() {
    	ArrayList<Monom> monoame1 = new ArrayList<Monom>();
    	ArrayList<Monom> monoame2 = new ArrayList<Monom>();
    	ArrayList<Monom> monoame3 = new ArrayList<Monom>();
    	Polinom quotient= new Polinom(monoame1);   //preparing the two polynomials for the result:
    	Polinom remainder= new Polinom(monoame2);  // the quotient and the remainder
    	Monom t = new Monom(0,0);
       	Polinom auxP = new Polinom(monoame3);  //auxiliary polynomial used in operations
       remainder = this.pol1; //initially the remainder is equal to the dividend
     
       while(getmaxRank(remainder) >= getmaxRank(pol2)) {        //divide the monomials with the highest
          t= divide2(getMonomMax(remainder),getMonomMax(pol2));  // exponent from the 2 polynomials
            quotient.addMonom(t);    //adding the result of the division to the quotient                           
	 
	if(auxP.monoame.isEmpty() == false) {
	    	auxP.monoame.clear();}      //clearing the auxiliary polynomial after every loop
	    	auxP.addMonom(t);         //we need it to contain only the result of the prior division
	    	
            Multiplication mlt = new Multiplication(auxP,pol2);
	    	auxP = mlt.multiply(); //multiplying the result of the division(a monomial) with the divisor
          	Subtraction  subt= new Subtraction(remainder,auxP);
	    	remainder = subt.subtract();  //we subtract from the current remainder the result 
	                                      //of the multiplication
	    	int i=0;
    while(i < remainder.monoame.size()) { //remove the monomials which have coefficients equal to 0
	     if(remainder.monoame.get(i).getCoef()==0)
	    		  remainder.monoame.get(i).setExp(-1);
	    	  i++; }}  
     String st = afisareQR(quotient,remainder); //display the result as a string (quotient | remainder)
       return st;}
}