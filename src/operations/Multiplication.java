package operations;

import java.util.ArrayList;

public class Multiplication {
	

	Polinom pol1;
	Polinom pol2;
	
	public Multiplication(Polinom pol1,Polinom pol2) {
		this.pol1 = pol1;
		this.pol2= pol2;
	}
	//function of 2 polynomials , computing the result of their multiplication 
	public Polinom multiply() {
		ArrayList<Monom> monoame1 = new ArrayList<Monom>();
		Polinom res= new Polinom(monoame1);
		for(Monom m1: pol1.monoame) {       //taking one monomial at a time,from the first polynomial, 
			for(Monom m2: pol2.monoame) {   //and multiplying it with every monomial from the second 
				double coeff =  m1.getCoef() * m2.getCoef();
				int exp = m1.getExp() + m2.getExp();  //the new exponent is the addition of the 2 current exp
				Monom monom3 = new Monom(coeff,exp);
				res.addMonom(monom3); //adding every newly created monomial to the result polynomial
			}
		}
       return res;
    }

}
