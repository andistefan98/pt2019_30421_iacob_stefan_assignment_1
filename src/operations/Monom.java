package operations;

public class Monom {
	
	private double coefficient;  //the coefficient is double ,in order to maintain the accuracy of the
	private int exp;             // integration and division operations 
	 
	 public Monom(double coef, int exp) {
		 this.coefficient = coef;             //the coefficient and exponent are the parameters of every monomial
		 this.exp = exp;                        
	 }
   //getters and setters for the coefficient and exponent , needed for the different operations
	 public double getCoef() {
		 return this.coefficient;
	 }
	 //get exponent 
	 public int getExp() {
		 return this.exp;
	 }
	 //modify current coefficient
	 public void setCoef(double coeff) {
		this.coefficient = coeff;
	 }
	 //modify current exponent
	 public void setExp(int exp) {
		 this.exp = exp;
	 }
}
