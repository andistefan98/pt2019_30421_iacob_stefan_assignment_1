package operations;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.text.DecimalFormat;
public class Polinom {

	//protected int maxExp ;
	//Monom head ;
	public ArrayList<Monom> monoame = new ArrayList<Monom>(); 
	DecimalFormat df2 = new DecimalFormat(".##");
	
	public Polinom( ArrayList<Monom> m1) { //each polynom is made up of 0 ,1 or more monomials
		this.monoame =m1;                  //monomials are stored in an ArrayList structure 
	}
	
    public Polinom addMonom(Monom monom1){ //method to add a monomial to the current polynomial
    	boolean ok= false;
    	int index=0;
    	for(Monom m1: monoame) {
    		if(m1.getExp()==monom1.getExp())  //if there already exists a monomial with the same exp
    			//then the new coefficient will be the sum of the 2: coefficient of the monomial
    			// to be added + coefficient of the monomial which has the same exponent 
    		{ok=true;                     
    		    index = monoame.indexOf(m1);  
    		    monom1.setCoef(monom1.getCoef()+ m1.getCoef()); 
    	}}
    	if(ok==false) //if no matching exponent is found , we just add the monomial as it is
      monoame.add(monom1);
    	if(ok==true) {
    		
    		monoame.set(index, monom1); //if a match is found, we change the coefficient of that monomial
    	}
    			
   return this;   
   }
    
  
    
    public void toStr() {
    	for(Monom m1: monoame) {
    		if(monoame.indexOf(m1)!=monoame.size()-1) {
    		if(m1.getExp()!=0) {
    			if(m1.getCoef()>0)
    		System.out.print(" +" + m1.getCoef() +"X^"+  m1.getExp()  +" +");
    			if(m1.getCoef()<0)
    		System.out.print( " " +m1.getCoef() +"X^"+  m1.getExp() );	}	
    		else {
    			System.out.print(" +" + m1.getCoef() );
    		}}
    		if(monoame.indexOf(m1)==monoame.size()-1) {
    			if(m1.getExp()!=0) {
    				if(m1.getCoef()>0)
    			System.out.print(" +" + m1.getCoef() +"X^"+  m1.getExp() );
    				if(m1.getCoef()<0)
    			System.out.print(" " + m1.getCoef() +"X^"+  m1.getExp() );}
    			else {
    				System.out.print(" " +m1.getCoef() +  "  ");

    			}
    		}
    	}
  }
    
    
    public String polToStr() {//method that converts a polynomial into a string 
    	int index=0;
    	String str=""; //empty string used to store result
    	if(monoame.size()==1 && monoame.get(0).getCoef()==0) {
       	str = "0"; // polynomial equal to zero
    		}
    	else {
 	 for(Monom m1: monoame) {
 		   if(m1.getExp() == 0) { // if the exponent is zero 
 			  if(m1.getCoef()>0)
 			   str= str.concat(" + " +df2.format(m1.getCoef()));
 			 if(m1.getCoef()<0) //only display the coefficient
 			   str= str.concat("  " +df2.format(m1.getCoef())); 
 		   }
 		   if(m1.getExp() != 0) { //if the exponent is not zero
 		   if(m1.getCoef()>0) //positive coefficient
 			   if((int)m1.getCoef()==0) { //coefficient from 0.01 to 0.99
 				  str =str.concat(" + 0"+ df2.format(m1.getCoef()) + "X" + "^" + m1.getExp() );
 			   }
 			   else { //coefficient of 1 or higher
 		   str =str.concat(" + "+ df2.format(m1.getCoef()) + "X" + "^" + m1.getExp() );}
 		   if(m1.getCoef()<0) { //negative coefficient
 		   str=str.concat(" "+ df2.format(m1.getCoef()) + "X" + "^" + m1.getExp() );   
 		           }
 		        }
 	        }
         }
 	   return str; //string that will be displayed in the GUI's output text field
    }
    
    //method that sorts the monomials of a polynomial with regard to the exponents
    public Polinom sortMon() { // biggest exponents come first in the returned polynomial
    	Collections.sort(monoame, 
    			(Monom a, Monom b) -> {
	               return -Integer.compare(a.getExp(), b.getExp());   //compare the exponents 
    			}                                                    //interchange in case of wrong order
    	);
    	return this;
    }
    
    
	 
    public static void main(String[] args) {
    	ArrayList<Monom> monoame1 = new ArrayList<Monom>();
    	ArrayList<Monom> monoame2 = new ArrayList<Monom>();
    	ArrayList<Monom> monoame3 = new ArrayList<Monom>();
    	Polinom poli1 = new Polinom(monoame1);
    	Polinom poli2 = new Polinom(monoame2);
    	Polinom res = new Polinom(monoame3);
    	Monom mon1 = new Monom(2,3);
    	Monom mon2 = new Monom(3,1);
    	Monom mon3 = new Monom(3,1);
    	Monom mon4 = new Monom(2,0);
    	poli1.addMonom(mon1);
        poli1.addMonom(mon2);
    	poli2.addMonom(mon3);
    	poli2.addMonom(mon4);
    	poli1.toStr();
    	poli2.toStr();
    	System.out.println("\n");
    	Division divis = new Division(poli1, poli2);
		String st =divis.divide4q();
    	Collections.sort(res.monoame, new Comparator<Monom>() {
    		@Override
			public int compare(Monom a, Monom b) {
				if(a.getExp() == b.getExp())
					return a.getExp();
                return a.getExp() < b.getExp() ? 1 : a.getExp() > b.getExp() ? -1:0;
			}
    	});
    	System.out.println("\n Result of div :  ");
    	System.out.println(st);
    }



}
