package operations;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Integration {

	Polinom pol1;

	
	public Integration(Polinom pol1) {
		this.pol1 = pol1;
	
	   }
	//function of one polynomial , computing the integral of it
	public Polinom integrate() {
		ArrayList<Monom> monoame1 = new ArrayList<Monom>();
		Polinom res = new Polinom(monoame1);  //result put in a 2nd polynomial
		for(Monom m1: pol1.monoame) {  
			
			
			if( m1.getExp() >= 1 ) {  //if exponent is at least 1
				m1.setExp(m1.getExp() + 1);   //using the rule of integration: incrementing the exponent
				m1.setCoef(m1.getCoef() / m1.getExp());   // dividing the current coefficient by the new exponent
				res.addMonom(m1);
			}
			
			if(m1.getExp() < 1) {  //if exponent is 0
				m1.setExp(1);
				res.addMonom(m1); //adding the newly created monomial to the result 
			}
			
		}
	   
	 return res;
}
}
