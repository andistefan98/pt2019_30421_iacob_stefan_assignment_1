package operations;

import java.util.ArrayList;

public class Subtraction {
	Polinom pol1;
	Polinom pol2;
	
	public Subtraction(Polinom pol1,Polinom pol2) {
		this.pol1 = pol1;
		this.pol2= pol2;
	}
	//function of two polynomials, computing the subtraction
	public Polinom subtract() {
		ArrayList<Monom> monoame1 = new ArrayList<Monom>();
		Polinom res = new Polinom(monoame1);  //result put in a 3rd polynomial
		boolean ok = false;
		boolean ok2= false;
		int index1 =0;
		
		for(Monom m1: pol1.monoame) {    //we take one monomial at a time from the first polynomial
			 for(Monom m2: pol2.monoame) {
				if (m1.getExp()==m2.getExp()) {    //search ,at first, if there is a monomial with equal
					ok=true;                       //rank in the second 
					index1 = pol2.monoame.indexOf(m2);
				}
			}
		     if(ok==true) {    //subtract the coefficients of the 2 monomials with equal exponent
		    	 m1.setCoef(m1.getCoef() - pol2.monoame.get(index1).getCoef());
		    	 res.monoame.add(m1);              
		    	  }
		     if(ok==false) {
		         res.monoame.add(m1);  //otherwise, add to the result the monomial with no match
		     }                         //(regarding the exponent)
		     ok=false;}
		
	    for(Monom m2: pol2.monoame) {
	    	for(Monom m1:res.monoame) {
	    		if(m2.getExp() == m1.getExp()) { 
	    			ok2=true; 
	    		}}
	    	if(ok2==false) {        //check for the monomials from the 2nd polynomial with no match
	    		Monom mNew = new Monom(m2.getCoef() * (-1),m2.getExp());
	    		res.monoame.add(mNew);    //putting the opposite of the coefficient in the result
	    	  }                           
	    	ok2=false;
	    }
	    return res;
	 }
	
}
