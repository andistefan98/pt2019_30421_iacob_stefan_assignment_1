package operations;

import java.util.ArrayList;

public class Addition {
	
	Polinom pol1;
	Polinom pol2;
	
	public Addition(Polinom pol1,Polinom pol2) {
		this.pol1 = pol1;
		this.pol2= pol2;
	}
	//function of two polynomials, computing the addition 
	public Polinom add() {
		ArrayList<Monom> monoame1 = new ArrayList<Monom>();
		Polinom res = new Polinom(monoame1);  //the result will be put in a 3rd polynomial
		boolean ok = false;
		boolean ok2= false;
		int index1 =0;
 
		for(Monom m1: pol1.monoame) {  //we take one monomial at a time from the first polynomial
			 for(Monom m2: pol2.monoame) {   
				if (m1.getExp()==m2.getExp()) {  //search ,at first, if there is a monomial with equal
					ok=true;                     //rank in the second 
					index1 = pol2.monoame.indexOf(m2);  //if yes, store it
				}}
		     if(ok==true) {  //if we found a monomial with the same rank
		    	 m1.setCoef(m1.getCoef() + pol2.monoame.get(index1).getCoef()); //only add coefficients
		    	 res.monoame.add(m1);   //and add the new monomial to the result
		    	  }
		     if(ok==false)
		         res.monoame.add(m1);    //otherwise just add the monomial ,as it is, to the result
		     ok=false;}       //keep the boolean on false for the next while loop
	    for(Monom m2: pol2.monoame) {    
	    	for(Monom m1:res.monoame) {
	    		if(m2.getExp() == m1.getExp()) {    //adding to the result the rest of monomials
	    			ok2=true;                       //from polynomial 2, who were not processed
	    		}}                                  //on the first for loops
	    	if(ok2==false) {
	    		res.monoame.add(m2);     //if it has not yet been used in a computation ,add
	    	  }
	    	ok2=false;} 
	    return res;
	 }

}