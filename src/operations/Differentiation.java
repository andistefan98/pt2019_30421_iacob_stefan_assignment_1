package operations;

import java.util.ArrayList;

public class Differentiation {
		Polinom pol1;
		
		public Differentiation(Polinom pol1) {
			this.pol1 = pol1;
		
		   }
	//function of only one polynomial
		public Polinom differentiate() {
			ArrayList<Monom> monoame1 = new ArrayList<Monom>();
			Polinom res = new Polinom(monoame1); //result stored ,as well, in another polynomial
			for(Monom m1: pol1.monoame) { //taking one monomial at a time
				if(m1.getExp() == 0) {
					m1.setCoef(0);       //if we had a monomial of exponent 0, the differentiation
					res.addMonom(m1);    // would be 0
				}
				if(m1.getExp() == 1) {
					m1.setExp(0);        //if the exponent is not 0, we use the usual rule:
					res.addMonom(m1);    // the new coefficient is the result of the multiplication 
				}                        //between the current coefficient and exp
				
				if( m1.getExp() > 1 ) {
					m1.setCoef(m1.getCoef() * m1.getExp());
					m1.setExp(m1.getExp() - 1);   //decrease the exponent by 1
					res.addMonom(m1);
				}
	
			}
		   return res;
   }

}
