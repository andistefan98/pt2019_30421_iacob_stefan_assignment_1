package GUI;
import java.awt.*;
import java.text.DecimalFormat;
import javax.swing.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.event.*;

import operations.Polinom;
import operations.Subtraction;
import operations.Addition;
import operations.Differentiation;
import operations.Division;
import operations.Integration;
import operations.Monom;
import operations.Multiplication;

public class GUIView extends JPanel{
	 
	
     GUIView()
	{
		
	}
	
	static Addition addit = null;
	
  public static void main(String[] args) {
	  
	  
	// building the actual GUI with components
		JFrame frame1 = new JFrame("PolynomialsOp");
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setSize(700, 270);
		
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();	
		JPanel panelNotice = new JPanel();
		JLabel  label1 = new JLabel("Insert 1st polynomial:  ");
		JLabel label2 = new JLabel("Insert 2nd polynomial:  ");
		JLabel label3 = new JLabel("Result of operation :  ");
		JLabel labelNotice = new JLabel("Notice: the monomials and signs should be separated by one blank space!");
		JTextField user_input1 = new JTextField(25);
		JTextField user_input2 = new JTextField(25);
		JTextField outputf = new JTextField(25);
		panel1.add(label1);
		panel1.add(user_input1);
		panel1.add(label2);
		panel1.add(user_input2);
		panel1.setLayout(new GridLayout(0,1));
	//adding buttons for the different operations performed
		JButton addbtn = new JButton("Add");
		JButton subbtn = new JButton("Subtract");
		JButton mulbtn = new JButton("Multiply");
		JButton divbtn = new JButton("Divide");
		JButton inbtn = new JButton("Integrate");
		JButton diffbtn = new JButton("Differentiate");
	// the coefficients of type double will be displayed with 2 decimals(for simplicity of reading)
		DecimalFormat df2 = new DecimalFormat(".##");
		
		panel2.add(addbtn);
		panel2.add(subbtn);
		panel2.add(mulbtn);
		panel2.add(divbtn);
		panel2.add(inbtn);
		panel2.add(diffbtn);
		
		panel3.add(label3);
		panel3.add(outputf);
		panelNotice.add(labelNotice);
		
		JPanel panelS = new JPanel();
		panelS.add(panel1);
		panelS.add(panel2);
		panelS.add(panel3);
		panelS.add(panelNotice);

		
		 final JPanel panel = new JPanel();
		
		frame1.setContentPane(panelS);
		frame1.setVisible(true);
	//division 
		divbtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
				String s1 = user_input1.getText();  //getting the user input
				String s2 = user_input2.getText();
			    Polinom p1 = processIn(s1);	// first string to polynomial form    	    
				Polinom p2 = processIn(s2); // second string to polynomial form
				Division div = new Division(p1,p2);
				String st3 = div.divide4q(); //division of the 2 
				 outputf.setText(st3);
			}
				catch(StringIndexOutOfBoundsException ex) {
					 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
					catch(NumberFormatException ex) {
						 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
					}
		}});
		
	//subtraction 	
         subbtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
				String s1 = user_input1.getText();  //user input
				String s2 = user_input2.getText();
			    Polinom p1 = processIn(s1);	 //string to polynomial
				Polinom p2 = processIn(s2);
				Subtraction subtr = new Subtraction(p1,p2);
				Polinom p3 = subtr.subtract();   //subtraction of the 2 polynomials
				 p3.sortMon();
				 //p3.toStr();
				 String stu = p3.polToStr();
				 outputf.setText(stu);
			}
				catch(StringIndexOutOfBoundsException ex) {
					 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
					catch(NumberFormatException ex) {
						 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
					}
		}});

    //integration of 1 polynomial ( the 1st one from the GUI)
        inbtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
				String s1 = user_input1.getText();  //only one input from the user
			    Polinom p1 = processIn(s1);		    //the 1st text field is processed	    
				Integration integr = new Integration(p1);
				Polinom p2 = integr.integrate();    //integrating the 1st polynomial
				 p2.sortMon();
				 String stu = p2.polToStr();   //putting the polynomial back to string format
				 outputf.setText(stu);       
			}
			catch(StringIndexOutOfBoundsException ex) {
				 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
				catch(NumberFormatException ex) {
					 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
		}});
     //differentiation of 1 polynomial ( the 1st one from the GUI)  
        diffbtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
				String s1 = user_input1.getText();
			    Polinom p1 = processIn(s1);		  //only the 1st text field is processed
				Differentiation dff = new Differentiation(p1);
				Polinom p2 = dff.differentiate(); //differentiating the 1st polynomial
				 p2.sortMon();
				 p2.toStr();
				 String stu = p2.polToStr();   //putting the polynomial back to string format
				 outputf.setText(stu);
			}
				catch(StringIndexOutOfBoundsException ex) {
					 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
					catch(NumberFormatException ex) {
						 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
					}
		}});
		
	//addition of two polynomials
       addbtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
				String s1 = user_input1.getText();    
				String s2 = user_input2.getText();     
			    Polinom p1 = processIn(s1);          //processing the 2 strings
				Polinom p2 = processIn(s2);          //provided by the user
				addit = new Addition(p1,p2);
				Polinom p3 = addit.add();         //addition of the 2
				 p3.sortMon();                 //sorting the result so as to have the biggest  
				 String stt = p3.polToStr();   //exponent in front of the polynomial
				 outputf.setText(stt);
			}
				catch(StringIndexOutOfBoundsException ex) {
					 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
					catch(NumberFormatException ex) {
						 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
					}
		}});
      //multiplication
        mulbtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
				String s1 = user_input1.getText();
				String s2 = user_input2.getText();
			    Polinom p1 = processIn(s1);    //process user input
				Polinom p2 = processIn(s2);
				Multiplication mul  = new Multiplication(p1,p2);
				Polinom p3 = mul.multiply();
				p3.sortMon();     //sorting the result ( biggest exponent in front)
				String stt = p3.polToStr();
				outputf.setText(stt);   //displaying the result in the 3rd text field
			}
				catch(StringIndexOutOfBoundsException ex) {
					 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
					catch(NumberFormatException ex) {
						 JOptionPane.showMessageDialog(panel, "Inavlid input !", "ERROR", JOptionPane.ERROR_MESSAGE);
					}
		}});
		
	 }
  
    //function used only for the first monomial taken from the String
    // this was necessary as we may have a '-' sign in front of the first monomial
    // in the case we do not have a '-' sign , we take '+' as default
    public static Polinom processStrFirst(String str,Polinom p1) {
    	 String nrString ="";
    	 if(str.length()==1) {
    		 String ss= ""+str.charAt(0);   //only one digit ,no sign
    		 p1.addMonom(new Monom(Integer.parseInt(ss),0));
    		}
    	if(str.length()>1) {
    		if(str.charAt(0) == '-') {    // negative coefficient
    			int i = 1; // i starts from 1 (as 0 is the negativitity sign)
    			  while(Character.isDigit(str.charAt(i))) { //build a string out of the digits
    	                 nrString = new StringBuilder(nrString).append(str.charAt(i)).toString();
    	                 i++; }
    	       	   int coefff= Integer.parseInt(nrString, 10); //string to actual integer 
    	       	   if(str.charAt(i)=='X') {  // if there is no X , the exponent must be 0
    	       	   String ss= ""+str.charAt(i+2);   //getting the exponent
    	       	   p1.addMonom(new Monom(-coefff,Integer.parseInt(ss)));}  //Monom(coef,exp)
    	       	   else {
    	       		p1.addMonom(new Monom(-coefff,0)); }}
    		  else {
    		  int i = 0;   //i starts from 0 
  			  while(Character.isDigit(str.charAt(i))) {
  	                 nrString = new StringBuilder(nrString).append(str.charAt(i)).toString();
  	                 i++; }
  	       	   int coefff= Integer.parseInt(nrString, 10);
  	       	   if(str.charAt(i)=='X') { // if there is no X, te exponent must be 0
  	       	   String ss= ""+str.charAt(i+2); //the next 2 positions of i must be 'X' and '^'
  	       	   p1.addMonom(new Monom(coefff,Integer.parseInt(ss))); }
  	       	   else {
  	       		p1.addMonom(new Monom(coefff,0)); //if we have no exponent explicitly written 
  	       	   }}}                                  //after the coefficient(power sign)
    	return p1; }
    
    //function used to process the polynomials from the GUI , except for the first monomial
    // processing 2 strings at a time: one is the sign , the second is the monomial
   public static Polinom processMonom(String[] mono, int index,Polinom p1) {
    	 String st = mono[index];  // the sign
		 String st2 = mono[index+1];  // the new monom
		 String nrString ="";
		 StringBuilder sb = new StringBuilder(nrString);  //stringBuilder used for the digits string
         Monom m2 = new Monom(0,0);
      if(mono[index+1].length()==1) {  //only one character -> one digit coefficient
       	   m2.setCoef(Character.getNumericValue(st2.charAt(0)));
       	   m2.setExp(0);
       	   p1.addMonom(m2);  }
      if(mono[index+1].length()!=1) {
       if(st.charAt(0)=='+') {  //positive coefficient
       	   int i=0;
       	   while(Character.isDigit(st2.charAt(i))) {
                 nrString = new StringBuilder(nrString).append(st2.charAt(i)).toString();
                 i++; } //build the string as long as there are digits in the coefficient
       	   int coefff= Integer.parseInt(nrString, 10);
       	   String ss= ""+st2.charAt(i+2); //add the exponent
       	   p1.addMonom(new Monom(coefff,Integer.parseInt(ss)));
       	 }
       else if(st.charAt(0) == '-'){  //negative coefficient
    	   int i=0;
       	   while(Character.isDigit(st2.charAt(i))==true) {
                 nrString = new StringBuilder(nrString).append(st2.charAt(i)).toString();
                 i++; }
       	   int coefff= Integer.parseInt(nrString);  // parsing the string to the actual int
       	   String ss= ""+st2.charAt(i+2); 
       	   p1.addMonom(new Monom(-coefff,Integer.parseInt(ss)));}} //negative 
             return p1; }
        
 
public static Polinom processIn(String str) throws StringIndexOutOfBoundsException {
		
		ArrayList<Monom> monoame1 = new ArrayList<Monom>();
		Polinom polinom1 = new Polinom(monoame1); //where our result is stored
		int index=1;  //index from 1, as case '0' is treated separately
		String[] mono = str.split(" ");   // split done when a blank space is encountered
	    String str1 = mono[0];     
		 polinom1 = processStrFirst(mono[0],polinom1); //process 1st monomials
	    
		 while(index < mono.length) { //as long as we have strings left in mono
	        processMonom(mono, index , polinom1); //process the rest of the monomials
	  
	        index = index + 2;  //1 for the sign , 1 for the actual monomial
         }
	               
		 return polinom1;
	    }
 
}
